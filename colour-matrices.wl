(* ::Package:: *)

(* ::Section:: *)
(*Set up*)


SetAttributes[fundamentalDelta,Orderless];
Format[fundamentalDynkinIndex]:=Subscript["D","T"];
Format[A[a__]]:=Subsuperscript["A",Length[List[a]],"tree"][a];
Format[star[a_]]:=Power[a,"*"];
applyRules[expr_,rules_]:=Module[
	{applyOnce},
	applyOnce[a_]:=Expand[a/.rules];
	Return[FixedPoint[applyOnce,expr]];
	];
fundamentalRules={
	T[a_,i1_,i2_]*T[a_,i3_,i4_]:>
		fundamentalDynkinIndex(fundamentalDelta[i1,i4]*fundamentalDelta[i2,i3]
								-(1/Nc)*fundamentalDelta[i1,i2]*fundamentalDelta[i3,i4]),
	fundamentalDelta[i_,i_]:>Nc,
	Power[fundamentalDelta[i1_,i2_],2]:>Nc,
	fundamentalDelta[i1_,i2_]*fundamentalDelta[i2_,i3_]:>fundamentalDelta[i1,i3],
	T[a1_,i1_,i2_] fundamentalDelta[i1_,i3_]:>T[a1,i3,i2],
	T[a1_,i1_,i2_] fundamentalDelta[i2_,i3_]:>T[a1,i1,i3],
	T[a_,i_,i_]->0
	};
adjointRules={	
	F[a_,b_,c_]:>Module[
		{i},
		(1/fundamentalDynkinIndex)*(T[b,i[1],i[2]]*T[a,i[2],i[3]]*T[c,i[3],i[1]]
		-T[b,i[4],i[5]]*T[c,i[5],i[6]]*T[a,i[6],i[4]])
		]
	};
nonCyclicalPermutations[n_]:=Prepend[#,1]&/@Permutations[Range[2,n]];


(* ::Section:: *)
(*Adjoint basis*)


adjointColourMatrixNormalisation[n_]:=
	Power[fundamentalDynkinIndex,n-2]*Power[Nc,n-2]*(Power[Nc,2]-1);
adjointColourMatrix[n_]:=Module[
	{
		len=Factorial[n-2],
		adjointCoefficients,
		adjointPartialAmplitudes,
		matrix
		},
	adjointCoefficients:=
		(Module[{A},Times@@
			{F[First[#],a[1],A[1]],
			Sequence@@Table[F[#[[j+1]],A[j],A[j+1]],{j,Length[#]-2}],
			F[Last[#],A[Length[#]-1],a[n]]}
			]&)/@
			((a/@#&)/@Permutations[Range[2,n-1]]);
	adjointPartialAmplitudes=A@@{1,Sequence@@#,n}&/@Permutations[Range[2,n-1]];
	matrix=
		Factor/@(
		applyRules[
		Table[
			adjointCoefficients[[i1]]*star[adjointCoefficients[[i2]]],
			{i1,len},{i2,len}
			],
		{
			star[a_*b_]:>star[a]*star[b],
			star[F[a_,b_,c_]]:>F[a,c,b],
			Sequence@@adjointRules,
			Sequence@@fundamentalRules
		}
		]/adjointColourMatrixNormalisation[n]);
	Return[{matrix,adjointPartialAmplitudes}];
	];
aNorm4=adjointColourMatrixNormalisation[4]/.fundamentalDynkinIndex->1/2
a4=adjointColourMatrix[4];
MatrixForm/@a4


(*a5=adjointColourMatrix[5];
MatrixForm/@a5*)


reducedSquareAmplitude[colourMatrixAndVector_]:=Module[
	{matrix,partialAmps},
	{matrix,partialAmps}=colourMatrixAndVector;
	Return[Dot[star/@partialAmps,matrix,partialAmps]//Expand];
	];
adjointAnswer=reducedSquareAmplitude[adjointColourMatrix[4]]


(* ::Section:: *)
(*Fundamental basis*)


fundamentalAmplitude[n_]:=
	Plus@@(fundamentalTrace@@#*A@@#&/@(nonCyclicalPermutations[n]));
processAmplitude[amplitude_]:=Module[
	{partialAmplitudes},
	partialAmplitudes=DeleteDuplicates[Cases[amplitude,_A,Infinity]];
	Return[{Coefficient[amplitude,#]&/@partialAmplitudes,partialAmplitudes}];
	]; 
fundamentalColourMatrixNormalisation[n_]:=
	(*2*Power[fundamentalDynkinIndex,2]*adjointColourMatrixNormalisation[n]*Power[Nc,-4];*)
	Power[fundamentalDynkinIndex,n]*Power[Nc,n-6]*(Power[Nc,2]-1);
fundamentalColourMatrix[fundamentalAmplitude_,n_,scale_]:=Module[
	{
		len,
		coefficients,
		partialAmplitudesVector,
		matrix,
		evaluateFundamentalTrace
		},
	{coefficients,partialAmplitudesVector}=processAmplitude[fundamentalAmplitude[n]];
	len=Length[coefficients];
	evaluateFundamentalTrace[t_]:=
		Times@@(Module[{i},Table[T[a[t[[j]]],i[j],i[If[j==Length[t],1,j+1]]],{j,Length[t]}]]);
	matrix=applyRules[
		Table[coefficients[[i1]]*star[coefficients[[i2]]],{i1,len},{i2,len}],
		{
			star[a_+ b_]:>star[a]+star[b],
			star[a_*b_]:>star[a]*star[b],
			star[a_fundamentalTrace]:>Reverse[a],
			a_fundamentalTrace:>
				Module[{pos=FirstPosition[a,Min[Apply[List,a]]][[1]]},Join[a[[pos;;]],a[[;;pos-1]]]],
			Power[t_fundamentalTrace,2]:>evaluateFundamentalTrace[t]*evaluateFundamentalTrace[t],
			star[-1]->-1
			}
		];
	Return[{
		Factor/@(applyRules[
			matrix,Join[{t_fundamentalTrace:>evaluateFundamentalTrace[t]},fundamentalRules]
			]/(scale*fundamentalColourMatrixNormalisation[n]))//Expand,
		partialAmplitudesVector
		}];
	];
reflectedFundamentalColourMatrix[n_,scale_]:=
	fundamentalColourMatrix[
		fundamentalAmplitude[#]/.
			a_A:>Condition[Power[-1,Length[a]]*Prepend[Reverse[Rest[a]],
							First[a]],First[Rest[a]]>Last[a]]&,
		n,
		scale
		];
fundamentalAmplitude[4]
fNorm4=fundamentalColourMatrixNormalisation[4]/.fundamentalDynkinIndex->1/2
{mat,vec}=fundamentalColourMatrix[fundamentalAmplitude,4,1];
MatrixForm[mat]
MatrixForm[vec]


DeleteDuplicates[Flatten[mat]]
%[[#]]->#-1&/@Range[3]
MatrixForm[mat/.%]
Table[Table[%[[i,j]],{i,j,6}],{j,6}]
Flatten[%]


List@@#&/@vec
#-1&/@%


f4=reflectedFundamentalColourMatrix[4,2];
MatrixForm/@f4


(*f5=reflectedFundamentalColourMatrix[5];
MatrixForm/@f5*)


MatrixForm/@fundamentalColourMatrix[fundamentalAmplitude,3,1]


f3=reflectedFundamentalColourMatrix[3];
MatrixForm/@f3
f3[[1,1,1]]*fundamentalColourMatrixNormalisation[3]/.fundamentalDynkinIndex->1/2
CForm[%/.Nc^2->Nc2]


(* ::Section::Closed:: *)
(*Kleiss - Kuijf relations*)


orderedPermutations[{a_,x___},{b_,y___},c___]:=
	Join[orderedPermutations[{x},{b,y},c,a],orderedPermutations[{a,x},{y},c,b]];
orderedPermutations[{x___},{y___},c___]:={{c,x,y}};
rule[n_]:=A[1,a__,n,b__]:>
	Power[-1,Length[{b}]]*Plus@@(A[1,Sequence@@#,n]&/@orderedPermutations[{a},{b}]);


A[1,2,4,3]/.rule[4]


fundamentalAnswer=reducedSquareAmplitude[reflectedFundamentalColourMatrix[4]]/.
	{A[1,2,4,3]:>(A[1,2,4,3]/.rule[4])}//.{star[a_+ b_]:>star[a]+star[b],star[-a_]:>-star[a]}//Expand


(* ::Section:: *)
(*Else*)


nonCyclicalPermutations[4]


fNorm4
aNorm4
fNorm4/aNorm4


ampF2 = 5.2444797312772562*10^-01
ampA2 = 1.2182140642365101*10^-02


ampF2
ampA2*Nc^4/2/.Nc->3






